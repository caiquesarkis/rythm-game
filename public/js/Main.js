//                                  Inicialização de variaveis globais

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75,window.innerWidth/window.innerHeight,0.1,1000);

const renderer = new THREE.WebGLRenderer({antialias:true,alpha:true});
renderer.setSize( window.innerWidth, window.innerHeight*0.85 );
document.body.appendChild( renderer.domElement );

const raycaster = new THREE.Raycaster();
const mouse = new THREE.Vector2();

const synth = new Tone.Synth().toMaster();


let colorDown  =  0xF4D8CD ;    
let colorUp  =   0xF15152;
let notes = ["C","D","E","F","G","A","B","C"]

// ---------------------------------------------------------------------------------
//                                 gerador de quadrados
function square(i,j,L){
    // parametros
    this.i = i;
    this.j = j;
    this.L = L;
    this.color = colorUp;
    this.geometry = new THREE.PlaneGeometry(this.L/this.j , this.L/this.i, 32 );
    this.material = new THREE.MeshBasicMaterial( {color: this.color , side: THREE.DoubleSide} );
    this.plane    =  new THREE.Mesh( this.geometry, this.material );
    scene.add( this.plane );   


    this.note = "";

    // metodo
    this.setPos = function(n,p){
        let spacing = 1.01;
        this.plane.position.set(spacing*n*this.L/this.i -12,spacing*p*this.L/this.j - 12.5,0)
    }

    this.setNote = function (n,p){
        this.note = notes[n] + p.toString()
    }

}

// ---------------------------------------------------------------------------------
//                                  cria quadrados
let grid    = [];
let row     = 8;
let columns = 8;
let area    = row*columns;
let size    = 25;

for (var i  = 0; i < area; i++) {
    grid[i] = new square(row,columns,size)
    
    //console.log(grid[i])
 }

// Organiza os quadrados em uma matriz
let j = 0;
let k = 0;
for (var i =0 ; i<area; i++){
    k +=1
    // Condições de contorno
    if (i%columns === 0){
        j += 1
        k = 0
    }
    grid[i].setPos(k, j);
    grid[i].setNote(k,j)
    
}





// ---------------------------------------------------------------------------------
//                                  Orbital camera
//var controls;
//controls = new THREE.OrbitControls(camera,renderer.domElement);
//controls.update();

// ---------------------------------------------------------------------------------
// Light config
let light = new THREE.PointLight(0xFFFFFF,1,500);
light.position.set(10,0,25);
const ambient = new THREE.AmbientLight(0x404040,5);
scene.add(ambient);

camera.position.z = 20;



// ---------------------------------------------------------------------------------
//                                  Click recgonition
function onMouseMove( event ) {

	// calculate mouse position in normalized device coordinates
	// (-1 to +1) for both components

	mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
	mouse.y = - ( event.clientY / window.innerHeight*0.85 ) * 2 + 1;

}

function drawer(setter) {

	// update the picking ray with the camera and mouse position
	raycaster.setFromCamera( mouse, camera );

	// calculate objects intersecting the picking ray
	const intersects = raycaster.intersectObjects( scene.children );

	for ( let i = 0; i < intersects.length; i ++ ) {
        
        if (setter === 1) {
            intersects[ i ].object.material.color.setHex( colorDown );
            
          
            for (let j=0 ; j < grid.length;j++){
                if (intersects[i].object.position === grid[j].plane.position){
                    synth.triggerAttackRelease(grid[j].note, Tone.now());
                    
                }
            }
            //
            
           
        }
        else{
            intersects[ i ].object.material.color.setHex( colorUp );
            synth.triggerRelease(Tone.now())
        }
        

	}

	renderer.render( scene, camera );

}
function changeColor(){
    drawer(1);
}
function changeColorBack(){
    drawer(0);
}

window.addEventListener( 'mousemove',   onMouseMove    , false);
window.addEventListener( 'mousedown',   changeColor    , false);
window.addEventListener( 'mouseup'  ,   changeColorBack, false);





//                                  Animation 
// ---------------------------------------------------------------------------------

function animate(){
    requestAnimationFrame(animate);  
    
    // Drawing and controls
    //controls.update();
    renderer.render(scene,camera);
}

animate()